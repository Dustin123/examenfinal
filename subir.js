const conexion = require('./cone_base_datos')

module.exports = app => {
    const connect = conexion

    app.post('/', (req, res) => {
        const cedula = req.body.cedula
        const apellidos = req.body.apellidos
        const nombres = req.body.nombres
        const direccion = req.body.direccion
        const telefono = req.body.telefono
        const correo_electrónico = req.body.correo_electrónico

        connect.query('insert into clientes SET ?', {
            cedula, apellidos, nombres, direccion, telefono, correo_electrónico
        }, (error, resultado) => {
            res.redirect('/')
        })
    })
    }
